function Crear_tabla() {
    var num = prompt("Introduce el tamaño de la tabla de potencias", "Numero");
    var body = document.getElementById('tabla');
    var tab = document.createElement('table');
    tab.style.width = '200px';
    tab.setAttribute('border', '1');
    var tab_bo = document.createElement('tbody');
    document.write("Numero Cuadrado Cubo");
    for(var i = 1; i<= num; i++) {
        var pot = Math.pow(i,2);
        var cubo = Math.pow(i,3);
        var tr = document.createElement('tr');
        for(var j = 0; j < 3; j++){
            var td = document.createElement('td');
            tr.appendChild(td);
            if(j==0){
                td.appendChild(document.createTextNode(i));
            }else if (j==1){
                td.appendChild(document.createTextNode(pot));
            }else{
                td.appendChild(document.createTextNode(cubo));
            }
        }
        tab_bo.appendChild(tr);
    }
    tab.appendChild(tab_bo);
    body.appendChild(tab);
}

function suma(){
    var x = Math.random();
    var y = Math.random();
    var t_inicio = performance.now();
    var respuesta = prompt("Dime la suma de "+ x +" y " + y, "Resultado");
    var tot = x + y;
    document.getElementById("suma").innerHTML = tot;
    if(respuesta == tot){
        var t_final = performance.now();
        document.getElementById("suma").innerHTML = "La suma fue correcta, te tardaste: "+(t_final -t_inicio).toFixed(3) +" ms";
    }else{
        var t_final = performance.now();
        document.getElementById("suma").innerHTML = "La suma fue incorrecta, te tardaste: "+(t_final -t_inicio).toFixed(3) +" ms";
    }
}

var arre = new Array (1, 4, -4, -69, 69, 96, 12, -1);
function contador(){
    var num_cero = 0;
    var num_po = 0;
    var num_neg = 0;
    for(var i=0; i < arre.length; i++){
        if(arre[i]==0){
            num_cero++;
        }else if(arre[i]>0){
            num_po++;
        }else if(arre[i]<0){
            num_neg++;
        }
    }
    document.getElementById("contador").innerHTML = "Numero de ceros: "+num_cero+ ", Numero de positivos: "+num_po+", Numero de negativos: "+num_neg;
}

function promedios(){
    var prom = 0;
    for(var i=0; i<arre.length; i++){
        prom = prom+arre[i];
    }
    prom=prom/arre.length;
    document.getElementById("promedio").innerHTML = "El promedio es: "+ prom;
}

function inverso(){
    var num = 766382627;
    num = num + "";
    var inv = num.split("").reverse().join("");
    document.getElementById("inverso").innerHTML = "El inverso de: "+num+", es: "+inv;
}

function no_caps(){
    var frase = prompt("Introduce la frase que desea quitar mayusculas", "Frase >:V");
    var no_caps = frase.toLowerCase();
    document.getElementById("boton 1").innerHTML = "Frase sin mayusculas: "+no_caps;
}

function caps(){
    var frase = prompt("Introduce la frase que desea en mayusculas", "Frase >:V");
    var caps = frase.toLocaleUpperCase();
    document.getElementById("boton 2").innerHTML = "Frase con mayusculas: "+caps;
}

function validador(){
    var contra1 = document.getElementById("contra1").value;
    var contra2 = document.getElementById("contra2").value;
    
    if(contra1 === contra2){
        document.getElementById("compras").style.display = "inline";
        document.getElementById("compras").style.visibility = "visible";
    } else if(contra1 === "posho" && contra2 === "1234"){
        document.body.style.backgroundImage = "url(Wallpaper2.png)";
    }
}

function cart(){
    var item_1 = 50*document.getElementById("item_1").value;
    var item_2 = 20*document.getElementById("item_2").value;
    var item_3 = 80*document.getElementById("item_3").value;
    var iva = (item_1+item_2+item_3)*.16;
    var tot = iva + item_1 + item_2 + item_3;
    document.getElementById("carro").innerHTML = "El total de la compra es $"+tot+"<br>Ensaladita $"+item_1+"<br>Gansito $"+item_2+"<br>Pulpito $"+item_3+"<br>Iva $"+iva;
}


function validador_pop(){
    var contra1 = document.getElementById("contra1").value;
    var contra2 = document.getElementById("contra2").value;
    
    if(contra1 === contra2){
        document.getElementById("correcto").style.display = "inline";
        document.getElementById("correcto").style.visibility = "visible";
        document.getElementById("incorrecto").style.display = "none";
        document.getElementById("incorrecto").style.visibility = "hidden";
    } else {
        document.getElementById("incorrecto").style.display = "inline";
        document.getElementById("incorrecto").style.visibility = "visible";
    }
}

var tic;

function mensajes_pop(){
    tic = setInterval(mensajes, 30000);
}

function mensajes(){
    var a = Math.floor((Math.random() * 5) + 1);
    if(a == 1){
        alert("Hola");
    } else if (a == 2){
        alert("Recuerda que si te gusta el lab me das 100 :v");
    } else if (a == 3){
        alert("Dame 100 plis :'v");
    } else if (a == 4){
        alert("Un 100 para este pobre estudiante");
    } else if (a == 5){
        alert("100?");
    }
}

/*Prueba drag*/
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}

var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 8
  });
}
