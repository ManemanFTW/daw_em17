--Laboratorio 15
--Manuel Flores
--A01206898

--Consulta de una tabla completa
SELECT * FROM materiales 

--Seleccion
SELECT * FROM materiales 
WHERE clave=1000 

--Proyeccion
SELECT clave, rfc, fecha 
FROM entregan

--Reunion natural
SELECT * FROM materiales, entregan 
WHERE materiales.clave = entregan.clave 

--Si alg�n material no ha se ha entregado �Aparecer�a en el resultado de esta consulta? 
--No

--Reunion con criterio especifico
SELECT * FROM entregan, proyectos 
WHERE entregan.numero < = proyectos.numero
--Regresa los datos en las que en ambas tablas se cumpla la condicion de que el numero en entregan sea menor o igual al numero del proyecto
--1188 rows

(SELECT * FROM entregan 
	WHERE clave=1450) 
union 
(SELECT * FROM entregan 
	WHERE clave=1300)
--3 rows
--Une los productos que se entregaron con clave 1450 y clave 1300

--En este punto descurbi que podia copiar el texto del laboratorio -_-

--�Cu�l ser�a una consulta que obtuviera el mismo resultado sin usar el operador Uni�n? Compru�balo.
--En el caso particular de esta consulta puedes solo consultar por el codigo 1300 siendo que no existe el codigo 1450

--Interseccion
(select clave from entregan where numero=5001) 
intersect 
(select clave from entregan where numero=5018)
--Regresa la clave de los materiales con el numero 5001 y 5018
--1 row

--Diferencia
(select * from entregan)
EXCEPT --En sql server no existe minus
(select * from entregan where clave=1000)
--Regresa todos los datos de entregan que no tengan la clave 1000
--129 rows

--Producto Cartesiano
select * from entregan, materiales
--Regresa una tabla con la todas las combinaciones entre materiales y entregan
--5808 rows

--Construcci�n de consultas a partir de una especificaci�n
SET DATEFORMAT dmy
SELECT descripcion
FROM Materiales M, Entregan E
WHERE M.clave = E.clave AND fecha > '31/12/1999' AND fecha < '1/1/2001'
--28 rows

--�Por qu� aparecen varias veces algunas descripciones de material?
--Por que hay materiales que se entregaron varias veces en el a�o 200

--Uso de calificador distinct
SELECT DISTINCT descripcion
FROM Materiales M, Entregan E
WHERE M.clave = E.clave AND fecha > '31/12/1999' AND fecha < '1/1/2001'
--22 rows

--�Qu� resultado obtienes en esta ocasi�n? 
--El distinctc evitan que se repitan los materiales que se entregaron varias veces

--Orenamientos
SELECT  numero, denominacion, fecha, cantidad
FROM Materiales M, Entregan E, Proyectos P
WHERE M.clave = E.clave AND E.numero = P.numero
ORDER BY fecha desc
--132 rows

--Operadores de Cadena
SELECT * FROM Materiales where Descripcion LIKE 'Si%' 
--2 rows

--�Qu� resultado obtienes?
--Se mostraron los materiales con el patron "si"

--Explica que hace el s�mbolo '%'
--Significa que a partir de ese punto acepta cualquier otra cadena mientras tenga la premisa correcta

--�Qu� sucede si la consulta fuera : LIKE 'Si' ?
--Solo regresaria los materiales que tengan como descripcion si

SELECT * FROM Materiales where Descripcion LIKE 'Si'
--0 rows

--�Qu� resultado obtienes?
--Nada por que no hay ningun material que tenga de descripcion si

--Explica a qu� se debe este comportamiento. 
-- No hay algun material con descripcion que solo tenga si 


DECLARE @foo varchar(40); 
DECLARE @bar varchar(40); 
SET @foo = '�Que resultado'; 
SET @bar = ' ���??? ' 
SET @foo += ' obtienes?'; 
PRINT @foo + @bar; 

--�Qu� resultado obtienes de ejecutar el siguiente c�digo?
--�Que resultado obtienes? ���???

--�Para qu� sirve DECLARE? 
--Para que declarar una variable

--�Cu�l es la funci�n de @foo? 
--Es el nombre de la variable

--�Que realiza el operador SET? 
--Le saigna un valor a la variable

SELECT RFC FROM Entregan WHERE RFC LIKE '[A-D]%'; 
--Regresa los RFC que empiezan con a,b,c y d
SELECT RFC FROM Entregan WHERE RFC LIKE '[^A]%'; 
--Regresa los RFC que no tienen A
SELECT Numero FROM Entregan WHERE Numero LIKE '___6'; 
--Regresa los numeros que terminan con 6 (seria lo mismo si pones %6??)

--Operadores Logicos
SELECT Clave,RFC,Numero,Fecha,Cantidad 
FROM Entregan 
WHERE Numero Between 5000 and 5010; 
--60 rows

--�C�mo filtrar�as rangos de fechas? 
--Puedes poner el primer dia del a�o al ultimo dia del a�o y lo unes con un between

SELECT RFC,Cantidad, Fecha,Numero 
FROM [Entregan] 
WHERE [Numero] Between 5000 and 5010 AND 
Exists ( SELECT [RFC] 
FROM [Proveedores] 
WHERE RazonSocial LIKE 'La%' and [Entregan].[RFC] = [Proveedores].[RFC])
--16 rows

--�Qu� hace la consulta? 
--Regresa los materiales entregados con numero entre 5000 y 5010 en la que la razon social de esos proyectos empieza con La

--�Qu� funci�n tiene el par�ntesis ( ) despu�s de EXISTS? 
--Marca el inicio de una subconsulta

SELECT RFC,Cantidad, Fecha,Numero
FROM [Entregan]
WHERE [Numero] Between 5000 and 5010
AND [Entregan].[RFC]
IN (SELECT [RFC]
    FROM [Proveedores]
    WHERE RazonSocial NOT LIKE 'La%')

SELECT TOP 2 * FROM Proyectos 
--�Qu� sucede con la  consulta?
--Regresa los primeros dos datos de la consulta

SELECT TOP numero FROM Proyectos 
--�Qu� sucede con la consulta?
--No funciona

--Modificando la estructura de una tabla existente 
ALTER TABLE materiales ADD PorcentajeImpuesto NUMERIC(6,2); 
UPDATE materiales SET PorcentajeImpuesto = 2*clave/1000; 

SELECT * FROM materiales

--�Qu� consulta usar�as para obtener el importe de las entregas es decir, el total en dinero de lo entregado, 
--basado en la cantidad de la entrega y el precio del material y el impuesto asignado?

SELECT SUM((Costo + (Costo * PorcentajeImpuesto/100))*E.Cantidad) AS Total
FROM Materiales M, Entregan E
WHERE M.clave IN(Select E.Clave
				 FROM Entregan)

--Creacion de vistas
Create view vistaMateriales
AS SELECT *
	FROM Materiales

Create view vistaProveedores
AS SELECT *
	FROM Proveedores

Create view entregasConClaveMil
AS SELECT *
	FROM entregan
	WHERE clave=1000

Create view descripEntregas
AS SELECT clave, rfc, fecha 
	FROM entregan

Create view matEntregados
AS SELECT * 
	FROM materiales, entregan 
	WHERE materiales.clave = entregan.clave

--Los materiales (clave y descripci�n) entregados al proyecto "M�xico sin ti no estamos completos". 
SELECT M.clave, descripcion
FROM Materiales M, Entregan E, Proyectos P
WHERE M.clave = E.clave AND P.numero = E.numero AND P.denominacion LIKE  'Mexico sin ti no estamos completos'
--3 rows

--Los materiales (clave y descripci�n) que han sido proporcionados por el proveedor "Acme tools". 
SELECT M.clave, M.descripcion
FROM Materiales M, Entregan E, Proveedores P
WHERE M.clave = E.clave AND P.rfc = E.rfc AND P.RazonSocial LIKE  'Acme tools'
--0 rows

 --El RFC de los proveedores que durante el 2000 entregaron en promedio cuando menos 300 materiales. 
SELECT RFC
FROM Entregan
WHERE fecha > '31/12/1999' AND fecha < '1/1/2001' AND Cantidad > = 300
--7 rows

--El Total entregado por cada material en el a�o 2000. 
SELECT SUM((Costo+(Costo*PorcentajeImpuesto/100))*E.Cantidad) AS TOTAL 
FROM Materiales M, Entregan E 
WHERE M.Clave IN (SELECT Clave 
				  From Entregan)
--42 rows

--La Clave del material m�s vendido durante el 2001. (se recomienda usar una vista intermedia para su soluci�n) 
SELECT TOP 1 clave
FROM Entregan E
WHERE fecha >= '01/01/2001' AND fecha < '1/1/2002'
ORDER BY E.cantidad desc
--1 row

--Productos que contienen el patr�n 'ub' en su nombre. 
CREATE VIEW productosPatUB(Descripcion)
AS SELECT Descripcion 
	FROM Materiales
	WHERE Descripcion LIKE '%ub%'
--12 rows

--Denominaci�n y suma del total a pagar para todos los proyectos. 
SELECT P.Denominacion, SUM((M.Costo * (1 + M.PorcentajeImpuesto)) * E.Cantidad ) AS Total
FROM Proyectos P INNER JOIN Entregan E ON P.Numero = E.Numero INNER JOIN Materiales M ON E.Clave = M.Clave
GROUP BY P.Denominacion
--20 rows

--Denominaci�n, RFC y RazonSocial de los proveedores que se suministran materiales al proyecto Televisa en acci�n que no se encuentran apoyando al proyecto Educando en Coahuila. (Solo usando vistas) 
CREATE VIEW ProveTele 
AS SELECT DISTINCT P.RazonSocial, P.RFC
FROM Proveedores P INNER JOIN Entregan ON P.RFC = Entregan.RFC INNER JOIN Proyectos P ON Entregan.Numero = P.Numero
WHERE P.Denominacion LIKE 'Televisa en acci�n'

CREATE VIEW ProveCoa 
AS SELECT DISTINCT P.RazonSocial, P.RFC
FROM Proveedores P INNER JOIN Entregan ON P.RFC = Entregan.RFC INNER JOIN Proyectos P ON Entregan.Numero = P.Numero
WHERE P.Denominacion LIKE 'Educando en Coahuila'

SELECT * FROM ProveTele
EXCEPT
SELECT * FROM ProveCoa
--2 rows


--Denominaci�n, RFC y RazonSocial de los proveedores que se suministran materiales al proyecto Televisa en acci�n que no se encuentran apoyando 
--al proyecto Educando en Coahuila. (Sin usar vistas, utiliza not in, in o exists) 
--
SELECT DISTINCT P.RazonSocial, P.RFC
FROM Proveedores P INNER JOIN Entregan ON P.RFC = Entregan.RFC INNER JOIN Proyectos ON Entregan.Numero = P.Numero
WHERE P.Denominacion LIKE 'Televisa en acci�n' AND P.RazonSocial NOT IN (SELECT DISTINCT P.RazonSocial
																		 FROM Proveedores P INNER JOIN Entregan E ON P.RFC = E.RFC INNER JOIN Proyectos ON E.Numero = P.Numero
																		 WHERE P.Denominacion LIKE 'Educando en Coahuila')
--2 rows

--Costo de los materiales y los Materiales que son entregados al proyecto Televisa en acci�n cuyos proveedores tambi�n suministran 
--materiales al proyecto Educando en Coahuila. 
SELECT M.Descripcion, SUM((M.Costo * (1 + M.PorcentajeImpuesto)) * Entregan.Cantidad ) AS Total
FROM Materiales M INNER JOIN Entregan ON M.Clave = Entregan.Clave
GROUP BY M.Descripcion
--42 rows