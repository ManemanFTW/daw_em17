IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'creaMaterial' AND type = 'P')
                            DROP PROCEDURE creaMaterial
                        GO
                        
                        CREATE PROCEDURE creaMaterial
                            @uclave NUMERIC(5,0),
                            @udescripcion VARCHAR(50),
                            @ucosto NUMERIC(8,2),
                            @uimpuesto NUMERIC(6,2)
                        AS
                            INSERT INTO Materiales VALUES(@uclave, @descripcion, @ucosto, @uimpuesto)
                        GO
--�Qu� hace el primer bloque del c�digo (bloque del IF)? 
--Elimina cualquier querry que se llame creaMaterial.

--�Para qu� sirve la instrucci�n GO?
--Sirve para marcar el fin de algun procedimiento

--�Explica que recibe como par�metro este Procedimiento y qu� tabla modifica? 
--Recibe la clave, descripcion, costo e impuesto para crear una tabla con esos parametros.

EXECUTE creaMaterial 5000,'Martillos Acme',250,15 
SELECT * FROM Materiales

IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'modificaMaterial' AND type = 'P')
                            DROP PROCEDURE modificaMaterial
                        GO
                        
                        CREATE PROCEDURE modificaMaterial
                            @uclave NUMERIC(5,0),
                            @udescripcion VARCHAR(50),
                            @ucosto NUMERIC(8,2),
                            @uimpuesto NUMERIC(6,2)
                        AS
                            UPDATE Materiales SET descripcion = @udescripcion, costo = @ucosto, PorcentajeImpuesto = @uimpuesto WHERE clave = @uclave;
                        GO

IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'eliminaMaterial' AND type = 'P')
                            DROP PROCEDURE eliminaMaterial
                        GO
                        
                        CREATE PROCEDURE eliminaMaterial
                            @uclave NUMERIC(5,0)
                        AS
                            DELETE FROM Materiales WHERE clave = @uclave;
                        GO

IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'creaProyecto' AND type = 'P')
                            DROP PROCEDURE creaProyecto
                        GO
                        
                        CREATE PROCEDURE creaProyecto
                            @unumero NUMERIC(5,0),
                            @udenominacion VARCHAR(50)
                        AS
                            INSERT INTO Proyectos VALUES(@unumero, @udenominacion)
                        GO


IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'modificaProyecto' AND type = 'P')
                            DROP PROCEDURE modificaProyecto
                        GO
                        
                        CREATE PROCEDURE modificaProyecto
                            @unumero NUMERIC(5,0),
                            @udenominacion VARCHAR(50)
                        AS
                            UPDATE Proyectos SET denominacion = @udenominacion WHERE numero = @unumero;
                        GO

IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'eliminaProyecto' AND type = 'P')
                            DROP PROCEDURE eliminaProyecto
                        GO
                        
                        CREATE PROCEDURE eliminaProyecto
                            @unumero NUMERIC(5,0)
                        AS
                            DELETE FROM Proyectos WHERE numero = @unumero;
                        GO

IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'creaProveedor' AND type = 'P')
                            DROP PROCEDURE creaProveedor
                        GO
                        
                        CREATE PROCEDURE creaProveedor
                            @uRFC VARCHAR(13),
                            @uRazonSocial VARCHAR(50)
                        AS
                            INSERT INTO Proveedores VALUES(@uRFC, @uRazonSocial)
                        GO


IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'modificaProveedor' AND type = 'P')
                            DROP PROCEDURE modificaProveedor
                        GO
                        
                        CREATE PROCEDURE modificaProveedor
                           @uRFC VARCHAR(13),
                           @uRazonSocial VARCHAR(50)
                        AS
                            UPDATE Proveedores SET RazonSocial = @uRazonSocial WHERE RFC = @uRFC;
                        GO

IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'eliminaProveedor' AND type = 'P')
                            DROP PROCEDURE eliminaProveedor
                        GO
                        
                        CREATE PROCEDURE eliminaProveedor
                            @uRFC VARCHAR(13)
                        AS
                            DELETE FROM Proveedores WHERE RFC = @uRFC;
                        GO

IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'creaEntrega' AND type = 'P')
                            DROP PROCEDURE creaEntrega
                        GO
                        
                        CREATE PROCEDURE creaEntrega
							@uclave NUMERIC(5,0),
							@uRFC VARCHAR(13),
							@unumero NUMERIC(5,0),
							@uFecha DATE,
							@uCantidad NUMERIC(5,0)
                        AS
                            INSERT INTO Entregan VALUES(@uclave, @uRFC, @unumero, @uFecha, @uCantidad)
                        GO


IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'modificaEntrega' AND type = 'P')
                            DROP PROCEDURE modificaEntrega
                        GO
                        
                        CREATE PROCEDURE modificaEntrega
                            @uclave NUMERIC(5,0),
							@uRFC VARCHAR(13),
			 			    @unumero NUMERIC(5,0),
						    @uFecha DATE,
							@uCantidad NUMERIC(5,0)
                        AS
                            UPDATE Entregan SET cantidad = @uCantidad WHERE clave = @uclave AND RFC = @uRFC AND numero = @unumero AND fecha = @uFecha
                        GO

IF EXISTS (SELECT name FROM sysobjects 
                                   WHERE name = 'eliminaEntrega' AND type = 'P')
                            DROP PROCEDURE eliminaEntrega
                        GO
                        
                        CREATE PROCEDURE eliminaEntrega
                            @uclave NUMERIC(5,0),
							@uRFC VARCHAR(13),
							@unumero NUMERIC(5,0),
							@uFecha DATE
                        AS
                            DELETE FROM Entregan WHERE clave = @uclave AND  RFC = @uRFC AND numero = @unumero AND fecha = @uFecha
                        GO

--Crear procedimientos para realizar consultas con par�metros 
IF EXISTS (SELECT name FROM sysobjects 
                                       WHERE name = 'queryMaterial' AND type = 'P')
                                DROP PROCEDURE queryMaterial
                            GO
                            
                            CREATE PROCEDURE queryMaterial
                                @udescripcion VARCHAR(50),
                                @ucosto NUMERIC(8,2)
                            
                            AS
                                SELECT * FROM Materiales WHERE descripcion 
                                LIKE '%'+@udescripcion+'%' AND costo > @ucosto 
                            GO
EXECUTE queryMaterial 'Lad',20
--Explica en tu reporte qu�recibe como par�metro este Procedimiento y qu� hace 
--Busca un producto con la misma descripcion pero con un costo menot del introducido.
