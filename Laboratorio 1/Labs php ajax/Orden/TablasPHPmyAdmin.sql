-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 11, 2017 at 06:49 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `bebidas`
--

CREATE TABLE IF NOT EXISTS `bebidas` (
  `Nombre` varchar(50) NOT NULL,
  `Tipo` varchar(25) DEFAULT NULL,
  `Precio` decimal(5,0) DEFAULT NULL,
  `Descripcion` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bebidas`
--

INSERT INTO `bebidas` (`Nombre`, `Tipo`, `Precio`, `Descripcion`) VALUES
('Jugo de Manzana', NULL, 15, NULL),
('Jugo de PiÃ±a', NULL, 32, NULL),
('Jugo de Toronja', NULL, 45, NULL),
('Jugo Naranja', 'Natural', 65, 'Jugo con naranja y pollo');

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(25) DEFAULT NULL,
  `Direccion` varchar(500) DEFAULT NULL,
  `Telefono` decimal(20,0) DEFAULT NULL,
  `Notas` varchar(250) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`id`, `Nombre`, `Direccion`, `Telefono`, `Notas`) VALUES
(1, 'Manuel Flores', 'Tec de Monterrey', 4151043322, 'Porton blanco con un perro bien lindo'),
(2, 'Pablo Escobar', '911', 0, 'Vive en un narco tunel'),
(3, 'Chavelo', '12229292', 0, 'Vive con diosito'),
(4, 'Luis Loco', 'Ecuador Quito', 4150287367, 'Vive en una casa de humitas'),
(5, 'Pablo Infante', 'Calle opalo #512 colonia balcon campestre qro, qro', 1206020, 'Vive en una casa abandonada y encantada por un chaman');

-- --------------------------------------------------------

--
-- Table structure for table `paninis`
--

CREATE TABLE IF NOT EXISTS `paninis` (
  `Nombre` varchar(50) NOT NULL,
  `Precio` decimal(5,0) DEFAULT NULL,
  `Descripcion` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paninis`
--

INSERT INTO `paninis` (`Nombre`, `Precio`, `Descripcion`) VALUES
('Panini de aguacate', 45, 'Panini con pan integral y aguacate de melcocha'),
('Panini de queso', 44, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `postres`
--

CREATE TABLE IF NOT EXISTS `postres` (
  `Nombre` varchar(50) NOT NULL,
  `Precio` decimal(5,0) DEFAULT NULL,
  `Descripcion` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `postres`
--

INSERT INTO `postres` (`Nombre`, `Precio`, `Descripcion`) VALUES
('Postre de manteca', 15, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bebidas`
--
ALTER TABLE `bebidas`
  ADD PRIMARY KEY (`Nombre`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paninis`
--
ALTER TABLE `paninis`
  ADD PRIMARY KEY (`Nombre`);

--
-- Indexes for table `postres`
--
ALTER TABLE `postres`
  ADD PRIMARY KEY (`Nombre`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
