<?php
    require_once "util.php";
    $result = getClientes();
        
    if(mysqli_num_rows($result) > 0){
        echo '<div class="responsive-table col l12 s12">';
        echo '<table>';
        echo '<thead>';
        echo '<tr>';
        echo '<th data-field="id">ID</th>';
        echo '<th data-field="id">Nombre</th>';
        echo '<th data-field="id">Direccion</th>';
        echo '<th data-field="id">Telefono</th>';
        echo '<th data-field="id">Notas</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';
        while ($row = mysqli_fetch_assoc($result)){
            echo '<tr>';
            echo '<td>' . $row["id"] . '</td>';
            echo '<td>' . $row["Nombre"] . '</td>';
            echo '<td>' . $row["Direccion"] . '</td>';
            echo '<td>' . $row["Telefono"] . '</td>';
            echo '<td>' . $row["Notas"] . '</td>';
            echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';
        echo '</div>';
    }
?>
