<?php
    session_start();
    require_once "util.php";
    if (isset($_POST["jugo"])) {
        //$_SESSION["jugo"][] = array($_POST["jugo"], $_POST["costo"]);
        nuevoJugo($_POST["jugo"], $_POST["costo"]);
    }
    else if (isset($_POST["panini"])) {
        //$_SESSION["panini"][] = array($_POST["panini"], $_POST["costo"]);
        nuevoPanini($_POST["panini"], $_POST["costo"]);
    }
    else if (isset($_POST["postre"])) {
        //$_SESSION["postre"][] = array($_POST["postre"], $_POST["costo"]);
        nuevoPostre($_POST["postre"], $_POST["costo"]);
    }
    
    if(isset($_POST["nombre"])){
        nuevoCliente($_POST["nombre"], $_POST["direccion"], $_POST["tel"], $_POST["notas"]);
        header("location:directorio.php");
    }

    if(isset($_POST["borrar_postre"])){
        borrarPostre($_POST["borrar_postre"]);
    }

    if(isset($_POST["borrar_jugo"])){
        borrarJugo($_POST["borrar_jugo"]);
    }
    
    if(isset($_POST["borrar_panini"])){
        borrarPanini($_POST["borrar_panini"]);
    }
    header("location:resgistro.php");
?>