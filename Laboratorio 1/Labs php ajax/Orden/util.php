<?php
    function conectDb(){
        $servername = "localhost";
        $username = "mane";
        $password = "";
        $dbname = "test";

        $con = mysqli_connect($servername, $username, $password, $dbname);
        if (!$con){
            die("Connection failed: " . mysqli_connect_error());
        }
        return $con;
    }

    function closeDb($mysql){
        mysqli_close($mysql);
    }
    
    function getNombClientes($nombre){
        $conn = conectDb();
        $sql = "SELECT Nombre FROM Cliente WHERE Nombre LIKE '".$nombre."%' ";
        $result = mysqli_query($conn, $sql);
        closeDb($conn);
        return $result;
    }

    function getClientes(){
        $conn = conectDb();
        $sql = "SELECT id, Nombre, Direccion, Telefono, Notas FROM Cliente";
        $result = mysqli_query($conn, $sql);
        closeDb($conn);
        return $result;
    }
    
    function getBebidas(){
        $conn = conectDb();
        $sql = "SELECT Nombre, Precio FROM Bebidas GROUP BY Nombre";
        $result = mysqli_query($conn, $sql);
        closeDb($conn);
        return $result;
    }

    function getPostres(){
        $conn = conectDb();
        $sql = "SELECT Nombre, Precio FROM Postres GROUP BY Nombre";
        $result = mysqli_query($conn, $sql);
        closeDb($conn);
        return $result;
    }

    function getPaninis(){
        $conn = conectDb();
        $sql = "SELECT Nombre, Precio FROM Paninis GROUP BY Nombre";
        $result = mysqli_query($conn, $sql);
        closeDb($conn);
        return $result;
    }


//nuevo Jugo
    function nuevoJugo($nombre, $precio) {
        
        $mysql = conectDb();
        
        // insert command specification 
        $query = "INSERT INTO Bebidas (Nombre, Precio) VALUES (?,?)";
        // Preparing the statement 
        if (!($statement = $mysql->prepare($query))) {
            die("Preparation failed: (" . $mysql->errno . ") " . $mysql->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("ss", $nombre, $precio)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
         // Executing the statement
         if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        } 

        closeDb($mysql); 
    }
//nuevo Panini
    function nuevoPanini($nombre, $precio) {
        
        $mysql = conectDb();
        
        // insert command specification 
        $query = "INSERT INTO Paninis (Nombre, Precio) VALUES (?,?)";
        // Preparing the statement 
        if (!($statement = $mysql->prepare($query))) {
            die("Preparation failed: (" . $mysql->errno . ") " . $mysql->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("ss", $nombre, $precio)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
         // Executing the statement
         if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        } 

        closeDb($mysql); 
    }

//nuevo Postre
    function nuevoPostre($nombre, $precio) {
        
        $mysql = conectDb();
        
        // insert command specification 
        $query = "INSERT INTO Postres (Nombre, Precio) VALUES (?,?)";
        // Preparing the statement 
        if (!($statement = $mysql->prepare($query))) {
            die("Preparation failed: (" . $mysql->errno . ") " . $mysql->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("ss", $nombre, $precio)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
         // Executing the statement
         if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        } 

        closeDb($mysql); 
    }

    function nuevoCliente($nombre, $direccion, $tel, $notas){
        $mysql = conectDb();
        $query = "INSERT INTO Cliente (Nombre, Direccion, Telefono, Notas) VALUES (?,?,?,?)";
        
        if (!($statement = $mysql->prepare($query))) {
            die("Preparation failed: (" . $mysql->errno . ") " . $mysql->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("ssss", $nombre, $direccion, $tel, $notas)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
         // Executing the statement
         if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        } 

        closeDb($mysql); 
    }
    
    function borrarJugo($nombre){
        $mysql = conectDb();
        
        $query = "DELETE FROM Bebidas WHERE Nombre = '".$nombre."'";
        $result = mysqli_query($mysql, $query);
        closeDb($mysql);
    }

    function borrarPanini($nombre){
        $mysql = conectDb();
        
        $query = "DELETE FROM Paninis WHERE Nombre = '".$nombre."'";
        $result = mysqli_query($mysql, $query);
        closeDb($mysql);
    }

    function borrarPostre($nombre){
        $mysql = conectDb();
        
        $query = "DELETE FROM Postres WHERE Nombre = '".$nombre."'";
        $result = mysqli_query($mysql, $query);
        closeDb($mysql);
    }
////////
?>