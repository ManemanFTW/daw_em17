BULK INSERT aequipo8.aequipo8.[Materiales]
   FROM 'e:\wwwroot\aequipo8\materiales.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )