BULK INSERT aequipo8.aequipo8.[Proveedores]
   FROM 'e:\wwwroot\aequipo8\proveedores.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
BULK INSERT aequipo8.aequipo8.[Proyectos]
   FROM 'e:\wwwroot\aequipo8\proyectos.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
SET DATEFORMAT dmy
BULK INSERT aequipo8.aequipo8.[Entregan]
   FROM 'e:\wwwroot\aequipo8\entregan.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )