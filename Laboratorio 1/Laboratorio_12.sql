--Ejercicio 1

drop TABLE entregan 
drop TABLE materiales 
drop TABLE proyectos 
drop TABLE Proveedores

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')
DROP TABLE Materiales 

CREATE TABLE Materiales 
( 
  Clave numeric(5) not null, 
  Descripcion varchar(50), 
  Costo numeric (8,2) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')
DROP TABLE Proveedores

CREATE TABLE Proveedores 
( 
  RFC char(13) not null, 
  RazonSocial varchar(50) 
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')
DROP TABLE Proyectos

CREATE TABLE Proyectos 
( 
  Numero numeric(5) not null, 
  Denominacion varchar(50) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')
DROP TABLE Entregan

CREATE TABLE Entregan 
( 
  Clave numeric(5) not null, 
  RFC char(13) not null, 
  Numero numeric(5) not null, 
  Fecha DateTime not null, 
  Cantidad numeric (8,2) 
) 

BULK INSERT aequipo8.aequipo8.[Materiales] 
  FROM 'e:\wwwroot\aequipo8\materiales.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

BULK INSERT aequipo8.aequipo8.[Proyectos] 
  FROM 'e:\wwwroot\aequipo8\proyectos.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

BULK INSERT aequipo8.aequipo8.[Proveedores] 
  FROM 'e:\wwwroot\aequipo8\proveedores.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

SET DATEFORMAT dmy -- especificar formato de la fecha 

BULK INSERT aequipo8.aequipo8.[Entregan] 
  FROM 'e:\wwwroot\aequipo8\entregan.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

--Ejercicio 2
INSERT INTO Materiales values(1000, 'xxx', 1000)
Delete from Materiales where Clave = 1000 and Costo = 1000
ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY (Clave) 
INSERT INTO Materiales values(1000, 'xxx', 1000)
--No puedes duplicar una llave primaria
sp_helpconstraint materiales
--Tiene como constraint la clave
ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero)
ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave, Numero, RFC, Fecha)
--ALTER TABLE tableName drop constraint ConstraintName

--sp_helpconstraint proyectos
--sp_helpconstraint proveedores
--sp_helpconstraint entregan

--Ejercicio 3
SELECT * from Materiales;
SELECT * from Proyectos;
SELECT * from Proveedores;
SELECT * from Entregan;

INSERT INTO entregan values (0, 'xxx', 0, '1-jan-02', 0);
Delete from Entregan where Clave = 0
--Agregar llave foranea a la rabla entregan
ALTER TABLE entregan add constraint cfentreganclave 
foreign key (clave) references materiales(clave);

--Ejercicio 4
INSERT INTO entregan values (1000, 'AAAA800101', 5000, GETDATE(), 0);
ALTER TABLE entregan add constraint Cantidad check (Cantidad > 0);

--Integridad referencial
--Es una clave externa que garantiza la sincronizacion entre dos tablas 
 