--Lab 18
--Manuel Flores
--A01206898

--La suma de las cantidades e importe total de todas las entregas realizadas durante el 97
SET DATEFORMAT DMY
SELECT SUM(Costo) AS Importe, SUM(Cantidad) AS Total 
FROM Entregan E, Materiales M
WHERE M.Clave = E.Clave AND Fecha BETWEEN '01/01/1997' AND '12/31/1997'

--Para cada proveedor, obtener la raz�n social del proveedor, n�mero de entregas e importe total de las entregas realizadas
SELECT DISTINCT p.RazonSocial RazonSocial, COUNT(*) TotalEntregas, SUM ((m.Costo + (m.Costo * (m.PorcentajeImpuesto/100)))* Entregan.Cantidad )as Total
FROM Proveedores p, Materiales m INNER JOIN Entregan e ON p.RFC = e.RFC INNER JOIN Materiales m ON e.Clave = m.Clave
GROUP BY RazonSocial

--Por cada material obtener la clave y descripci�n del material, la cantidad total entregada, la m�nima cantidad entregada, la m�xima cantidad entregada, el importe total de las entregas de aquellos materiales en los que la cantidad promedio entregada sea mayor a 400
SELECT m.Clave as Clave_Materiales, m.Descripcion Descripci�n_Material, SUM(e.Cantidad) Cantidad_Entregados, MIN(e.Cantidad) Min_Entregada, MAX(e.Cantidad) Max_Entregada, SUM ((m.Costo + (m.Costo * (m.PorcentajeImpuesto/100)))* e.Cantidad )as Total
FROM Materiales m INNER JOIN Entregan e ON m.Clave = e.Clave
GROUP BY m.Clave, m.Descripcion
HAVING AVG(e.Cantidad) > 400

--Para cada proveedor, indicar su raz�n social y mostrar la cantidad promedio de cada material entregado, detallando la clave y descripci�n del material, excluyendo aquellos proveedores para los que la cantidad promedio sea menor a 500
SELECT p.RazonSocial Razon_Social, AVG(e.Cantidad) Promedio_Entregados, e.Clave Clave_Mat, m.Descripcion Descrip_Mat
FROM Proveedores p INNER JOIN Entregan e ON p.RFC = e.RFC INNER JOIN Materiales m ON e.Clave = m.Clave
GROUP BY p.RazonSocial, e.Clave, m.Descripcion
HAVING AVG(e.Cantidad) > 500

--Mostrar en una solo consulta los mismos datos que en la consulta anterior pero para dos grupos de proveedores: aquellos para los que la cantidad promedio entregada es menor a 370 y aquellos para los que la cantidad promedio entregada sea mayor a 450
SELECT p.RazonSocial Razon_Social, AVG(e.Cantidad) Prom_Entregas, e.Clave Clave_Mat, m.Descripcion Desc_Mat
FROM Proveedores p INNER JOIN Entregan e ON p.RFC = e.RFC INNER JOIN Materiales m ON e.Clave = m.Clave
GROUP BY p.RazonSocial, e.Clave, m.Descripcion
HAVING AVG(e.Cantidad) BETWEEN 370 AND 450

--Agregar Materiales
INSERT INTO Materiales VALUES (1679,'Caldera', 70,1.4);
INSERT INTO Materiales VALUES (1680,'Placa de Marmol', 86,2.5);
INSERT INTO Materiales VALUES (1681,'Pilar de Marmol', 120.45,3);
INSERT INTO Materiales VALUES (1682,'Bloque de Uranio', 77.12,1.15);
INSERT INTO Materiales VALUES (1683,'Pilar de Uranio', 44.32,3.12);

--Clave y descripci�n de los materiales que nunca han sido entregados
SELECT m.Clave Clave_Mat, m.Descripcion Desc_Mat
FROM Materiales m
WHERE m.Clave NOT IN(SELECT e.Clave 
					 FROM Entregan e)

--Raz�n social de los proveedores que han realizado entregas tanto al proyecto 'Vamos M�xico' como al proyecto 'Quer�taro Limpio'
SELECT DISTINCT p.RazonSocial Razon_Social
FROM Proveedores p INNER JOIN Entregan e ON p.RFC = e.RFC INNER JOIN Proyectos p ON Entregan.Numero = p.Numero
WHERE p.Denominacion = 'Vamos Mexico' AND p.RazonSocial IN(SELECT p.RazonSocial
														   FROM Proveedores p INNER JOIN Entregan e ON p.RFC = e.RFC INNER JOIN Proyectos p ON Entregan.Numero = p.Numero
														   WHERE p.Denominacion = 'Queretaro Limpio')

--Descripci�n de los materiales que nunca han sido entregados al proyecto 'CIT Yucat�n'
SELECT m.Descripcion
FROM Materiales m, Proyectos p, Entregan e
WHERE m.Clave = e.Clave AND p.Numero = e.Numero AND p.Denominacion <> 'CIT Yucatan'

--Raz�n social y promedio de cantidad entregada de los proveedores cuyo promedio de cantidad entregada es mayor al promedio de la cantidad entregada por el proveedor con el RFC 'VAGO780901'
SELECT p.RazonSocial Razon_Social, AVG(e.Cantidad) Prom_Cantidad
FROM Proveedores p, Entregan e
WHERE p.RFC = e.RFC
GROUP BY p.RazonSocial
HAVING AVG(e.Cantidad) > (SELECT AVG(e.Cantidad)
						  FROM Proveedores p, Entregan e
						  WHERE p.RFC = e.RFC AND p.RFC = 'VAGO780901')

--RFC, raz�n social de los proveedores que participaron en el proyecto 'Infonavit Durango' y cuyas cantidades totales entregadas en el 2000 fueron mayores a las cantidades totales entregadas en el 2001
SET DATEFORMAT DMY
SELECT p.RFC, p.RazonSocial
FROM Proveedores p, Entregan e, Proyectos
WHERE e.Fecha >= '01/01/00' AND e.Fecha <= '31/12/00' AND e.Numero = Proyectos.Numero AND p.RFC = e.RFC AND Proyectos.Denominacion = 'Infonavit Durango'
HAVING SUM(e.Cantidad) > (SELECT SUM(e.Cantidad)
						  FROM Proveedores p, Entregan e, Proyectos
						  WHERE e.Fecha BETWEEN '01/01/01' AND '31/12/01' AND e.Numero = Proyectos.Numero AND p.RFC = e.RFC AND Proyectos.Denominacion = 'Infonavit Durango')