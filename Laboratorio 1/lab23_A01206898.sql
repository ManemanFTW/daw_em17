--Manuel Flores A01206898
--Laboratorio 22

--Tablas
Create Table Clientes_Banca(
	NoCuenta varchar(5) NOT NULL PRIMARY KEY,
	Nombre varchar(30),
	Saldo numeric(10,2)
)
Create Table Realizan(
	NoCuenta varchar(5),
	ClaveM varchar(2),
	Fecha datetime,
	Monto numeric(10,2)
)
Create Table Tipos_Movimiento(
	ClaveM varchar(5) NOT NULL PRIMARY KEY,
	Descripcion varchar(30)
)

BEGIN TRANSACTION PRUEBA1
INSERT INTO Clientes_Banca VALUES('001', 'Manuel Rios Maldonado', 9000);
INSERT INTO Clientes_Banca VALUES('002', 'Pablo Perez Ortiz', 5000);
INSERT INTO Clientes_Banca VALUES('003', 'Luis Flores Alvarado', 8000);
COMMIT TRANSACTION PRUEBA1 

SELECT* FROM Clientes_Banca;

--�Que pasa cuando deseas realizar esta consulta? 
--Se hace la consulta

BEGIN TRANSACTION PRUEBA2
INSERT INTO CLIENTES_BANCA VALUES('004','Ricardo Rios Maldonado',19000); 
INSERT INTO CLIENTES_BANCA VALUES('005','Pablo Ortiz Arana',15000); 
INSERT INTO CLIENTES_BANCA VALUES('006','Luis Manuel Alvarado',18000);

SELECT * FROM CLIENTES_BANCA

--�Qu� pasa cuando deseas realizar esta consulta? 
--El gestor esta esperando a que se termine la anterior para poder empezar

SELECT * FROM CLIENTES_BANCA where NoCuenta='001'

--Explica por qu� ocurre dicho evento. 
--La tranzaccion todabia no termina

ROLLBACK TRANSACTION PRUEBA2;

SELECT * FROM CLIENTES_BANCA

--�Qu� ocurri� y por qu�? 
--La tranzaccion hizo rollback


BEGIN TRANSACTION PRUEBA3
INSERT INTO TIPOS_MOVIMIENTO VALUES('A','Retiro Cajero Automatico');
INSERT INTO TIPOS_MOVIMIENTO VALUES('B','Deposito Ventanilla');
COMMIT TRANSACTION PRUEBA3
BEGIN TRANSACTION PRUEBA4
INSERT INTO Realizan VALUES('001','A',GETDATE(),500);
UPDATE CLIENTES_BANCA SET SALDO = SALDO -500
WHERE NoCuenta='001'
COMMIT TRANSACTION PRUEBA4


BEGIN TRANSACTION PRUEBA5
INSERT INTO CLIENTES_BANCA VALUES('005','Rosa Ruiz Maldonado',9000);
INSERT INTO CLIENTES_BANCA VALUES('006','Luis Camino Ortiz',5000);
INSERT INTO CLIENTES_BANCA VALUES('001','Oscar Perez Alvarado',8000);


IF @@ERROR = 0
COMMIT TRANSACTION PRUEBA5
ELSE
BEGIN
PRINT 'A transaction needs to be rolled back'
ROLLBACK TRANSACTION PRUEBA5
END

--�Para qu� sirve el comando @@ERROR revisa la ayuda en l�nea? 
--Detecta si hubo un error durante la ejecucion de esa prueba 

--�Explica qu� hace la transacci�n? 
--Agrega cuatro cosas a la tabla pero si encuentra un error no los manda

--�Hubo alguna modificaci�n en la tabla? Explica qu� pas� y por qu�. 
--Ninguna por que se detecto un error y eso impidio que se mandaran los campos


CREATE PROCEDURE Registra_Retiro_Cajero
                @uNoCuenta varchar(5),
		@uFecha datetime,
		@UMonto numeric(10,2)
            AS
		BEGIN TRANSACTION RegistraRetiro
                INSERT INTO Realizan VALUES(@uNoCuenta, 'A', @uFecha, @uMonto);
			UPDATE Clientes_Banca Set Saldo=Saldo-@UMonto where NoCuenta=@uNoCuenta;
			IF @@ERROR = 0
				COMMIT TRANSACTION RegistraRetiro
			ELSE
			BEGIN
				PRINT 'A transaction needs to be rolled back'
				ROLLBACK TRANSACTION RegistraRetiro
			END	
            GO

CREATE PROCEDURE Registra_Deposito_Ventanilla
                @uNoCuenta varchar(5),
		@uFecha datetime,
		@UMonto numeric(10,2)
            AS
		BEGIN TRANSACTION RegistraDeposito
                INSERT INTO Realizan VALUES(@uNoCuenta, 'B', @uFecha, @uMonto);
			UPDATE Clientes_Banca Set Saldo=Saldo+@UMonto where NoCuenta=@uNoCuenta;
			IF @@ERROR = 0
				COMMIT TRANSACTION RegistraDeposito
			ELSE
			BEGIN
				PRINT 'A transaction needs to be rolled back'
				ROLLBACK TRANSACTION RegistraDeposito
			END		
            GO